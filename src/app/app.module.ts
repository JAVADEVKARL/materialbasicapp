import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialModule} from './material/material.module';
import {AutocompleteComponent} from './autocomplete/autocomplete.component';
import { BadgeComponent } from './badge/badge.component';
import { ButtonComponent } from './button/button.component';
import { ButtonToggleComponent } from './button-toggle/button-toggle.component';
import { CardsComponent } from './cards/cards.component';
import { CheckboxComponent } from './checkbox/checkbox.component';
import { DatatableComponent } from './datatable/datatable.component';
import { DatepickerComponent } from './datepicker/datepicker.component';
import { DialogComponent } from './dialog/dialog.component';
import { DialogExampleComponent } from './dialog-example/dialog-example.component';
import { ExpansionPanelComponent } from './expansion-panel/expansion-panel.component';
import { GridListComponent } from './grid-list/grid-list.component';
import { IconsComponent } from './icons/icons.component';
import { InputComponent } from './input/input.component';
import { ListComponent } from './list/list.component';
import { MenuComponent } from './menu/menu.component';
import { ProgressSpinnerComponent } from './progress-spinner/progress-spinner.component';
import { RadiobuttonComponent } from './radiobutton/radiobutton.component';
import { SelectComponent } from './select/select.component';
import { SidenavComponent } from './sidenav/sidenav.component';
import {CustomSnackBarComponent, SnackbarComponent} from './snackbar/snackbar.component';
import { StepperComponent } from './stepper/stepper.component';
import { TabsComponent } from './tabs/tabs.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { TypographyComponent } from './typography/typography.component';
import { VirtualScrollingComponent } from './virtual-scrolling/virtual-scrolling.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ScrollingModule} from '@angular/cdk/scrolling';
import { TooltipComponent } from './tooltip/tooltip.component';



@NgModule({
  declarations: [
    AppComponent,
    AutocompleteComponent,
    BadgeComponent,
    ButtonComponent,
    ButtonToggleComponent,
    CardsComponent,
    CheckboxComponent,
    DatatableComponent,
    DatepickerComponent,
    DialogComponent,
    DialogExampleComponent,
    ExpansionPanelComponent,
    GridListComponent,
    IconsComponent,
    InputComponent,
    ListComponent,
    MenuComponent,
    ProgressSpinnerComponent,
    RadiobuttonComponent,
    SelectComponent,
    SidenavComponent,
    SnackbarComponent,
    StepperComponent,
    TabsComponent,
    ToolbarComponent,
    TypographyComponent,
    VirtualScrollingComponent,
    CustomSnackBarComponent,
    TooltipComponent
  ],
  entryComponents: [CustomSnackBarComponent, DialogExampleComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    ScrollingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
